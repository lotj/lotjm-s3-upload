(ns lotjm-s3-upload.core
  (:import [java.io File InputStreamReader BufferedReader]
           [org.jets3t.service S3Service]
           [org.jets3t.service.impl.rest.httpclient RestS3Service]
           [org.jets3t.service.model S3Bucket S3Object]
           [org.jets3t.service.security AWSCredentials])
  (:require [clojure.java.jdbc :as sql])
  (:gen-class))

;; PROPERTIES

(def props (java.util.Properties.))
(.load props (java.io.FileInputStream. "aws.properties"))

(defn property
  "Loads a property from the aws.properties file or returns error string"
  [p]
  (.getProperty props p (str "MISSING PROPERTY '" p "'")))

;; AWS FUNCTIONS

(def s3service
  (RestS3Service. (AWSCredentials.
                   (property "awsAccessKey") (property "awsSecretKey"))))

(defn upload-file
  "Uploads a file with a base path, id, name and mimetype"
  [path id name mimetype]
  (let [bucketname (property "awsBucket")
        bucket (.getBucket s3service bucketname)
        s3obj  (S3Object. (File. (str path File/separator id)))
        s3name (str "files/" (quot id 1000) "/" id "/" name)]
    ;; eg. "files/123/123456/my document.doc"
    (do
      (.setContentType s3obj mimetype)
      (print (str "uploading " id " ... "))
      (.putObject s3service bucket s3obj)
      (print (str "'" s3name "' ..."))
      (.moveObject s3service bucketname (.getName s3obj)  bucketname (S3Object. s3name) false)
      (println " done."))))

;; FILE FUNCTIONS

(defn list-files
  "List of plain (non-dir) files from a given path"
  [path]
  (sort
   (map #(.getName %) ;; get the names
        (filter
         #(not (.isDirectory %)) ;; which are not directories
         (.listFiles (File. path)))))) ;; from the list of files

;; DB CONNECTION

(def mysql-db {:subprotocol "mysql" :subname (property "dbsubname")
               :user (property "dbuser") :password (property "dbpassword")})

(defn get-file-metadata
  "Obtain metadata from the associated file id"
  [id]
  (sql/with-connection mysql-db
    (sql/with-query-results rows
      ["SELECT name, mimetype FROM document_info WHERE document_id = ?" id]
      {:name (:name (first rows)) :mimetype (:mimetype (first rows))})))

;; MAIN FUNCTIONS

(defn upload-contents
  "Uploads the contents of a whole folder"
  [path]
  (let [files (list-files path)]
    (doall
     (map
      (fn [filename]
        (do
          (let [id (Integer. filename)
                metadata (get-file-metadata id)]
            (upload-file path id (:name metadata) (:mimetype metadata)))))
      files))))

(defn show-help
  []
  (println "Usage: java -jar lotjm-s3-upload.jar <folder>")
  (println "-> Uploads the contents of the folder to the bucket defined on aws.properties"))

;; 2014.04.11 - required for HDK #1601

(defn full-s3-path [id]
  (let [filename (:name (get-file-metadata id))]
    (str "files/" (int (/ id 1000)) "/" id "/" filename)))

(defn download-file [id]
  (let [bucketname (property "awsBucket")
        bucket     (.getBucket s3service bucketname)
        path       (full-s3-path id)
        s3obj      (.getObject s3service bucket path)
        buffer     (byte-array 1024)]
    (println "reading" id "...")
    (with-open [istream (.getDataInputStream s3obj)
                ostream (clojure.java.io/output-stream (str id))]
      (loop [c (.read istream buffer)]
        (if (not= c -1)
          (do
            (.write ostream buffer 0 c)
            (recur (.read istream buffer))))))
    (println "saved.")))

;; Main entry point

(defn -main[& args]
  (if (seq args)
    (upload-contents (first args))
    (show-help)))

