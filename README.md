# lotjm-s3-upload

A tool to upload to Amazon S3 the files uploaded to LOTJm, by Denis Fuenzalida

## Usage

    java -jar lotjm-s3-upload.jar <folder>

## License

Copyright © 2013 Law of the Jungle Pty. Ltd.

